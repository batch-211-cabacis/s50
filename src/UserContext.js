import React from 'react'

// Creation of context object

const UserContext = React.createContext();

// The "Provider" allows other components to consume/use the context object and supply the neccessary information needed

export const UserProvider = UserContext.Provider;

export default UserContext;