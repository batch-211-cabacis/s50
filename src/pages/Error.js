// import {Button} from 'react-bootstrap';
// import {Link} from 'react-router-dom';

  
// export default function PageNotFound(){
//     return (
//         <div>
//         <h1>404 - Page Not Found</h1>
//         <p>The page you are looking for cannot be found.</p>
//         <Link to ='/'>
//             <Button variant="primary">Back Home</Button>
//         </Link>
//         </div>

//         )
// }

// Activty solotuion

import Banner from '../components/Banner';

export default function Error(){
    
    const data  = {

        title: "404 - Not Found",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back Home"
    }

    return (
        // "props" name is up to the developer but we need to 
        <Banner bannerProp = {data}/>
        )

}

